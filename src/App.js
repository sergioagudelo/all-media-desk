import * as React from 'react';

import FirstPoint from './FirstPoint';
import SecondPoint from './SecondPoint';
import ThirdPoint from './ThirdPoint';
import './App.css';

function App() {

  return (
    <React.Fragment>
      <h1>First point</h1>
      <FirstPoint />
      <br/>
      <h1>Second point</h1>
      <SecondPoint/>
      <br/>
      <h1>Third point</h1>
      <ThirdPoint/>
    </React.Fragment>
  );
}

export default App;
