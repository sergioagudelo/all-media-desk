import * as React from "react";

import "./App.css";

function FirstPoint() {
  const [result, setResult] = React.useState(0);
  const [expression, setExpression] = React.useState("(12-2*3.3)+5-9/3+(3+9)");

  const calcMe = (str) => {
    const noWsStr = str.replace(/\s/g, "");
    // if (noWsStr.startsWith('-')) {
    //   const tempStr = noWsStr.slice(1);
    //   console.log(1111, tempStr)
    // }
    const operators = noWsStr.replace(/[\d.,]/g, "").split("");
    const operands = noWsStr
      .replace(/[+/%*-]/g, " ")
      .replace(/\,/g, ".")
      .split(" ")
      .map(parseFloat)
      .filter((it) => it);

    // if (operators.length >= operands.length){
    //   throw new Error('Operators qty must be lesser than operands qty')
    // };

    while (operators.includes("*")) {
      let opIndex = operators.indexOf("*");
      operands.splice(opIndex, 2, operands[opIndex] * operands[opIndex + 1]);
      operators.splice(opIndex, 1);
    }
    while (operators.includes("/")) {
      let opIndex = operators.indexOf("/");
      operands.splice(opIndex, 2, operands[opIndex] / operands[opIndex + 1]);
      operators.splice(opIndex, 1);
    }

    let result = operands[0];
    for (let i = 0; i < operators.length; i++) {
      operators[i] === "+"
        ? (result += operands[i + 1])
        : (result -= operands[i + 1]);
    }
    return result;
  };

  const replacer = (str, p1, p2, offset, s) => {
    // if (str.replace(/\(([-+]?[0-9]*\.?[0-9]+[\/\+\-\*])+([-+]?[0-9]*\.?[0-9]+)\)/g){
    //   replacer
    // }
    const strWithoutBrackets = str.slice(1, -1);
    // let acumBracketsresult;
    // if (strWithoutBrackets.includes('(')) {
    //   acumBracketsresult = str.replace(/\(([-+]?[0-9]*\.?[0-9]+[\/\+\-\*])+([-+]?[0-9]*\.?[0-9]+)\)/g, replacer)
    //   console.log
    // }
    // console.log(str, p1, p2, offset, s)
    return calcMe(strWithoutBrackets);
  };

  const recursiveBrackets = (str) => {
    const bracketsresult = str.replace(
      /\(([-+]?[0-9]*\.?[0-9]+[\/\+\-\*])+([-+]?[0-9]*\.?[0-9]+)\)/g,
      replacer
    );
    setResult(calcMe(bracketsresult));
  };

  React.useEffect(() => {
    recursiveBrackets(expression);
  }, []);

  return (
    <React.Fragment>
      <input
        onInput={(event) => setExpression(event.target.value)}
        value={expression}
        style={{ width: "100%" }}
      />
      <button
        onClick={() => {
          recursiveBrackets(expression);
        }}
      >
        Calculate
      </button>
      <div>{result}</div>
    </React.Fragment>
  );
}

export default FirstPoint;
