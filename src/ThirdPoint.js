import * as React from "react";

import "./App.css";

function ThirdPoint() {
const [expression, setExpression] = React.useState('4*(x*x)+(5.3*x)+5');

  const calcMe = (str) => {
    const noWsStr = str.replace(/\s/g, '');
    // if (noWsStr.startsWith('-')) {
    //   const tempStr = noWsStr.slice(1);
    //   console.log(1111, tempStr)
    // }
    const operators = noWsStr.replace(/[\d.,]/g, '').split('');
    const operands = noWsStr
      .replace(/[+/%*-]/g, ' ')
      .replace(/\,/g, '.')
      .split(' ')
      .map(parseFloat)
      .filter((it) => it);

    // if (operators.length >= operands.length){
    //   throw new Error('Operators qty must be lesser than operands qty')
    // };

    while (operators.includes('*')) {
      let opIndex = operators.indexOf('*');
      operands.splice(opIndex, 2, operands[opIndex] * operands[opIndex + 1]);
      operators.splice(opIndex, 1);
    }
    while (operators.includes('/')) {
      let opIndex = operators.indexOf('/');
      operands.splice(opIndex, 2, operands[opIndex] / operands[opIndex + 1]);
      operators.splice(opIndex, 1);
    }

    let result = operands[0];
    for (let i = 0; i < operators.length; i++) {
      operators[i] === '+'
        ? (result += operands[i + 1])
        : (result -= operands[i + 1]);
    }
    return result;
  };

  const replacer = (str, p1, p2, offset, s) => {
    // if (str.replace(/\(([-+]?[0-9]*\.?[0-9]+[\/\+\-\*])+([-+]?[0-9]*\.?[0-9]+)\)/g){
    //   replacer
    // }
    const strWithoutBrackets = str.slice(1, -1);
    // let acumBracketsresult;
    // if (strWithoutBrackets.includes('(')) {
    //   acumBracketsresult = str.replace(/\(([-+]?[0-9]*\.?[0-9]+[\/\+\-\*])+([-+]?[0-9]*\.?[0-9]+)\)/g, replacer)
    //   console.log
    // }
    // console.log(str, p1, p2, offset, s)
    return calcMe(strWithoutBrackets);
  };

  const recursiveBrackets = (str) => {
    const bracketsresult = str.replace(
      /\(([-+]?[0-9]*\.?[0-9]+[\/\+\-\*])+([-+]?[0-9]*\.?[0-9]+)\)/g,
      replacer
    );
    return calcMe(bracketsresult);
  };

  const handleCalculateClick = () => {
    const POINT_SIZE = 1.5;
    const INITIAL_X = 0;
    const LAST_X = 200;
    const DELTA_X = 1;

    const canvas = document.getElementById('myCanvas');
    const context = canvas.getContext('2d');

    // const mathFunction = (x) => recursiveBrackets(`4*(${x}*${x})+(5.3*${x})+5`);
    const mathFunction = (x) => recursiveBrackets(expression.replace(/x/g, x));

    const drawPoint = (x, y) => {
      context.beginPath();
      context.arc(x, y, POINT_SIZE, 0, 2 * Math.PI, true);
      context.fill();
    };

    context.clearRect(0, 0, canvas.width, canvas.height);

    for (let x = INITIAL_X; x <= LAST_X; x += DELTA_X) {
      const y = mathFunction(x);
      drawPoint(x, y);
    }
  };

  return (
    <React.Fragment>
      <input
        onInput={(event) => setExpression(event.target.value)}
        value={expression}
        style={{ width: '100%' }}
      />
      <button onClick={handleCalculateClick}>Calculate</button>
      <canvas
        id='myCanvas'
        width='1000'
        height='500'
        style={{ border: '1px solid #000000', transform: 'rotateX(180deg)' }}
      >
        Your browser does not support the HTML canvas tag.
      </canvas>
    </React.Fragment>
  );
}

export default ThirdPoint;
