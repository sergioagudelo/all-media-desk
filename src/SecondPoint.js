import * as React from "react";

import "./App.css";

function SecondPoint() {
  const [result, setResult] = React.useState(0);
  const [number, setNumber] = React.useState(0);
  
  const findPeterNumber = (currentNumber) => {
    const checkedNumbers = []
    let index = null;

    for (let i = 0; i < currentNumber.length - 1; i++) {
      console.log(currentNumber[i], currentNumber[i + 1]);
      if (currentNumber[i] < currentNumber[i + 1]) {
        checkedNumbers.push(+currentNumber[i]);
        if (!currentNumber[i + 2]) {
          checkedNumbers.push(+currentNumber[i + 1]);
        }
        continue;
      }
      
      if (currentNumber[i] > currentNumber[i + 1]) {
        checkedNumbers.push(+currentNumber[i] - 1);
        index = i;
        break;
      }
    }

    const generatedNines = [...new Array(currentNumber.length - 1 - index)]
    for (let i = 0; i < generatedNines.length; i++) {
      checkedNumbers.push(9)
    }

    setResult(checkedNumbers.map(String).join().replace(/,/g, ''));

  }

  return (
    <React.Fragment>
      <input onInput={(event) => setNumber(event.target.value)} value={number} style={{width: '100%'}}/>
      <button onClick={() => { findPeterNumber(number) }}>Calculate Peter Number</button>
      {result}
      <div>{result}</div>
    </React.Fragment>
  );
}

export default SecondPoint;
